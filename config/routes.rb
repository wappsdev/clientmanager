Rails.application.routes.draw do
  resources :tags

  devise_for :users
  get 'welcome/index'

  root 'welcome#index'

  resources :clients
end
