# ClientManager #

My initial personal Rails project prior to a focused application, MISMS. ClientManager will be
a more robust, wider user base application that will be continued post MISMS completion.

Features: Devise authorization, Bootstrap for styling, and initial client management (MVC)

Platform/framework: Ruby on Rails, Javascript, jQuery, HTML, CSS, Bootstrap, Postgresql