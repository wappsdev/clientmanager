# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
# User.create!(email: 'user01@blah.com', password: 'user0111', password_confirmation: 'user0111')
# User.create!(email: 'user02@blah.com', password: 'user0222', password_confirmation: 'user0222')
# User.create!(email: 'user03@blah.com', password: 'user0333', password_confirmation: 'user0333')

Tag.create(name: 'Insurance', description: 'Insurance Client', type_of: 'client', user_id: 1)
Tag.create(name: 'Mortgage', description: 'Mortgage Client', type_of: 'client', user_id: 1)
Tag.create(name: 'Insurance', description: 'Insurance client', type_of: 'client', user_id: 2)
Tag.create(name: 'Mortgage', description: 'Mortgage Client', type_of: 'client', user_id: 3)
Tag.create(name: 'Friend', description: 'Friend', type_of: 'client', user_id: 3)
