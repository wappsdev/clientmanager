class CreateClients < ActiveRecord::Migration
  def change
    create_table :clients do |t|
      t.string :first_name
      t.string :last_name
      t.string :middle_initial
      t.string :name_suffix
      t.string :gender
      t.date   :dob
      t.string :phone_number
      t.string :email_address
      t.string :address_line1
      t.string :address_line2
      t.string :city
      t.string :state
      t.string :zip_code
      t.text   :description

      t.timestamps null: false
    end
  end
end
