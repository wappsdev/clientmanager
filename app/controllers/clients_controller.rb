class ClientsController < ApplicationController
  before_action :find_client, only: [:show, :edit, :update, :destroy]
  def index
    # @clients = Client.all.order("last_name ASC, first_name ASC")
    if user_signed_in?
      @clients = Client.where(user_id: current_user).order("last_name ASC, first_name ASC")
    else
      # redirect_to invalid_form
      redirect_to new_user_session_path
    end
  end

  def show
  end

  def new
    # @client = Client.new
    @client = current_user.clients.build
  end

  def create
    # @client = Client.new(client_params)
    @client = current_user.clients.build(client_params)

    if @client.save
      redirect_to @client
    else
      render 'new'
    end
  end

  def edit
  end

  def update
    if @client.update(client_params)
      redirect_to @client
    else
      render 'edit'
    end
  end

  def destroy
    @client.destroy
    redirect_to clients_path
  end

  private

  def find_client
    # @client = Client.find(params[:id])
    @client = current_user.clients.find(params[:id])
  end

  def client_params
    params.require(:client).permit(:first_name, :last_name, :middle_initial, :name_suffix, :gender, :dob, :phone_number, :email_address, :address_line1, :address_line2, :city, :state, :zip_code, :description)
  end
end
