class Client < ActiveRecord::Base
  belongs_to :user

  validates :last_name, presence: true
  validates :first_name, presence: true
end
